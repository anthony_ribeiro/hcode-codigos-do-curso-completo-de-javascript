var express = require('express');
var formidable = require('formidable');
var router = express.Router();
var fs = require('fs');

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/upload', (req, res) => {

  var form = new formidable.IncomingForm({
    uploadDir: './uploads',
    keepExtensions: true
  });

  form.parse(req, (err, fields, files) => {

    res.json({
      files
    });

  });

});

router.delete('/file', (req, res) => {

  var form = new formidable.IncomingForm({
    uploadDir: './uploads',
    keepExtensions: true
  });

  form.parse(req, (err, fields, files) => {

    let filepath = './' + fields.path;

    if (fs.existsSync(filepath)) {

      fs.unlink(filepath, err => {

        c

      });

    } else {

      res.status(404).json({
        err:'File not found'
      });

    }

  });

});

router.get('/file', (req, res)=>{

  let filepath = './' + req.query.path;

  if (fs.existsSync(filepath)) {

    fs.readFile(filepath, (err, data)=>{

      if (err) {
        console.error(err);
        res.status(400).json({
          err
        });
      } else {

        res.status(200).end(data);

      }

    });

  } else {
    res.status(404).json({
      err: 'File not found'
    });
  }

});

module.exports = router;